package com.picz.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.ui.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.picz.datamodel.Group;
import com.picz.datamodel.PiczDatabase;
import com.picz.datamodel.User;

public class JoinRequestActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String LINK="https://picz-app.s3.amazonaws.com/joinGroup/@";

    private Button btnJoin,btnCancel;
    private TextView tvGroupName;
    private ImageView ivGroupImage;
    private String groupId;
    private Group currentGroup;
    private String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_request);
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        String appLinkAction = appLinkIntent.getAction();
        Uri appLinkData = appLinkIntent.getData();

//        Toast.makeText(getApplicationContext(),"success join group " + appLinkData.toString(),Toast.LENGTH_LONG).show();

        Toolbar myToolbar = (Toolbar) findViewById(R.id.join_group_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        myToolbar.setTitle("Join Group");

        btnJoin=findViewById(R.id.join_group_join);
        btnCancel=findViewById(R.id.join_group_cancel);
        tvGroupName=findViewById(R.id.join_group_name);
        ivGroupImage=findViewById(R.id.join_group_image);
        btnJoin.setOnClickListener(this);
        btnCancel.setOnClickListener(this);

        String []splitLink=appLinkData.toString().split("@");
        groupId= splitLink[1];
//        21MCVTQ45w4MmXjDl4xtT
        PiczDatabase.getInstance().getSingleGroup(groupId).addOnSuccessListener(new OnSuccessListener<Group>() {
            @Override
            public void onSuccess(Group group) {
                currentGroup=group;
                tvGroupName.setText(currentGroup.getGroupName());
                RequestOptions requestOptions=new RequestOptions()
                        .placeholder(R.mipmap.ic_launcher_round);
                Glide.with(getApplicationContext())
                        .load(Uri.parse(currentGroup.getGroupImageUri()))
                        .apply(requestOptions)
                        .circleCrop()
                        .into(ivGroupImage);
            }
        });
        if(FirebaseAuth.getInstance().getCurrentUser()!=null)
            userId=FirebaseAuth.getInstance().getCurrentUser().getUid();
        else {
            startActivity(new Intent(JoinRequestActivity.this, SignInActivity.class));

        }
    }

    @Override
    public void onClick(View v) {

        if(v==btnJoin){
            PiczDatabase.getInstance().isUserExists(groupId,userId).addOnSuccessListener(new OnSuccessListener<Boolean>() {
                @Override
                public void onSuccess(Boolean aBoolean) {
                    if(aBoolean)
                    {
                        PiczDatabase.getInstance().addUserToGroup(userId,groupId);
                        PiczDatabase.getInstance().addGroupToUser(currentGroup,userId);

                        Toast.makeText(getApplicationContext(),"You have joined group "+currentGroup.getGroupName(),Toast.LENGTH_SHORT).show();
                        Intent i=new Intent(JoinRequestActivity.this,MainActivity.class);
                        startActivity(i);

                    }
                    else {
                        Toast.makeText(getApplicationContext(),"You have already joined this group",Toast.LENGTH_SHORT).show();

                    }
                }
            });

        }

        if(v==btnCancel){
            finish();
        }
    }
}
