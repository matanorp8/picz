package com.picz.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.ui.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.FirebaseAuth;
import com.picz.datamodel.PiczDatabase;
import com.picz.datamodel.User;

import java.util.ArrayList;

public class GroupDetailsActivity extends AppCompatActivity{

    private TextView groupTitle;
    private ImageView groupImage;
    private RecyclerView recyclerViewUsers;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<String> userNames;
    private ArrayList<String> usersId;
    private TextView groupCount;
    private boolean isCurrentAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_details);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.group_details_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        myToolbar.setTitle("Group details");

        groupCount=findViewById(R.id.group_count);
        groupTitle=findViewById(R.id.group_details_title);
        groupImage=findViewById(R.id.group_details_image);
        recyclerViewUsers=findViewById(R.id.recycler_View_group_details);
        recyclerViewUsers.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);

        RequestOptions requestOptions=new RequestOptions()
                .placeholder(R.mipmap.ic_launcher_round);
        Glide.with(getApplicationContext())
                .load(Uri.parse(getIntent().getStringExtra("group image")))
                .apply(requestOptions)
                .into(groupImage);
        groupTitle.setText(getIntent().getStringExtra("group title"));

        PiczDatabase.getInstance().isAdmin(getIntent().getStringExtra("group id"), FirebaseAuth.getInstance().getCurrentUser().getUid()).addOnSuccessListener(new OnSuccessListener<Boolean>() {
            @Override
            public void onSuccess(Boolean aBoolean) {
                if(aBoolean)
                {
                    isCurrentAdmin=true;
                }
                else
                    isCurrentAdmin=false;
            }
        });


        PiczDatabase.getInstance().getUsersFromGroup(getIntent().getStringExtra("group id")).addOnSuccessListener(new OnSuccessListener<ArrayList<String>>() {
            @Override
            public void onSuccess(final ArrayList<String> strings) {
                usersId=strings;
                PiczDatabase.getInstance().getUserNameList(strings).addOnSuccessListener(new OnSuccessListener<ArrayList<String>>() {
                    @Override
                    public void onSuccess(ArrayList<String> strings2) {
                        userNames=strings2;
                        adapter=new AddUsersAdapter(getApplicationContext(),userNames, getIntent().getStringExtra("group id"), strings);
                        recyclerViewUsers.setLayoutManager(layoutManager);
                        recyclerViewUsers.setAdapter(adapter);
                        if(strings2.size()==1)
                            groupCount.setText("1 member");
                        else
                            groupCount.setText(strings2.size()+" members");

                    }
                });
            }
        });






    }

}