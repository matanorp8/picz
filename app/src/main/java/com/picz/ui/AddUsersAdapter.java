package com.picz.ui;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.ui.R;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.picz.datamodel.Image;
import com.picz.datamodel.PiczDatabase;
import com.picz.datamodel.User;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class AddUsersAdapter extends RecyclerView.Adapter<AddUsersAdapter.AddUserViewHolder> {
    private ArrayList<String> usersList;
    private String groupId;
    private ArrayList<String> usersId;
    private Context mContext;


    public AddUsersAdapter(Context mContext,ArrayList<String> usersList, String groupId, ArrayList<String> usersId) {
        this.usersList = usersList;
        this.groupId = groupId;
        this.usersId = usersId;
        this.mContext=mContext;
    }

    @NonNull
    @Override
    public AddUserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_users_item, parent, false);
        AddUserViewHolder addUserViewHolder = new AddUserViewHolder(v);
        return addUserViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final AddUserViewHolder holder, int position) {
        String currentItem = usersList.get(position);
        String currentId = usersId.get(position);
        holder.mTextView.setText(currentItem);
        PiczDatabase.getInstance().isAdmin(groupId, currentId).addOnSuccessListener(new OnSuccessListener<Boolean>() {
            @Override
            public void onSuccess(Boolean aBoolean) {
                if (aBoolean) {
                    holder.admin.setVisibility(View.VISIBLE);
                }
            }
        });
        PiczDatabase.getInstance().getUserImage(currentId).addOnSuccessListener(new OnSuccessListener<String>() {
            @Override
            public void onSuccess(String s) {
                RequestOptions requestOptions=new RequestOptions()
                        .placeholder(R.mipmap.ic_launcher_round);
                Glide.with(mContext)
                        .load(Uri.parse(s))
                        .apply(requestOptions)
                        .into(holder.mImageView);
            }
        });



    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }

    public static class AddUserViewHolder extends RecyclerView.ViewHolder  {

        public TextView mTextView;
        public ImageView mImageView;
        public ImageView admin;
        public LinearLayout layout;
        public CardView cardView;

        public AddUserViewHolder(@NonNull View itemView) {
            super(itemView);
            mTextView = itemView.findViewById(R.id.textview_username);
            mImageView = itemView.findViewById(R.id.image_view_add_user);
            admin = itemView.findViewById(R.id.admin);
            layout = itemView.findViewById(R.id.layout_details);
            cardView = itemView.findViewById(R.id.cardview);
        }
    }

}



