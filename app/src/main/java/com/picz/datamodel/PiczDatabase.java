package com.picz.datamodel;

import android.app.Activity;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.concurrent.Executor;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class PiczDatabase {

    private FirebaseFirestore piczDatabaseFirestore;
    private static PiczDatabase instance = null;

    public PiczDatabase() {
        piczDatabaseFirestore = FirebaseFirestore.getInstance();
    }

    public static PiczDatabase getInstance() {
        if (instance == null) {
            instance = new PiczDatabase();
        }
        return instance;
    }

    public void CreateGroup(Group group) {

        instance.piczDatabaseFirestore.collection("groups").
                document(group.getGroupId()).set(group);
    }

    public void deleteGroup(Group group) {
    }

    public void addUser(User user)
    {
        instance.piczDatabaseFirestore.collection("users").
                document(user.getFirebaseUserId()).set(user);
    }

    public Task<User> getCurrentUser() {
        final TaskCompletionSource s = new TaskCompletionSource<User>();
        String currentId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        instance.piczDatabaseFirestore.collection("users").document(currentId).get()
            .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                @Override
                public void onSuccess(DocumentSnapshot documentSnapshot) {
                    User user = documentSnapshot.toObject(User.class);
                    s.setResult(user);
                }
            });

        return s.getTask();

    }

    public void addGroupToUser(Group group,String id)
    {
        try
        {
            instance.piczDatabaseFirestore.collection("users")
                    .document(id).update("groups", FieldValue.arrayUnion(group.getGroupId()));
        }catch (Exception e)
        {
            System.out.println(e);
        }

    }

    public Task<ArrayList<User>> getUsers()
    {
        final TaskCompletionSource s = new TaskCompletionSource<User>();
        instance.piczDatabaseFirestore.collection("users").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                final ArrayList<User> arrayResult=new ArrayList<>();

                for(DocumentSnapshot documentSnapshot:queryDocumentSnapshots.getDocuments())
                {
                    User user=documentSnapshot.toObject(User.class);
                    arrayResult.add(user);
                }
                s.setResult(arrayResult);
            }
        });
        return s.getTask();
    }


    public Task<ArrayList<Group>> getGroups(String userId)
    {
        final TaskCompletionSource s = new TaskCompletionSource<User>();

        final ArrayList<Group> arrayResult=new ArrayList<>();
        instance.piczDatabaseFirestore.collection("users").document(userId).get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        User user = documentSnapshot.toObject(User.class);
                        final ArrayList<String> groups=user.getGroups();

                        instance.piczDatabaseFirestore.collection("groups").
                                whereArrayContains("usersList",user.getFirebaseUserId()).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                final ArrayList<Group> arrayResult=new ArrayList<>();

                                for(DocumentSnapshot documentSnapshot:queryDocumentSnapshots.getDocuments())
                                {
                                    Group group=documentSnapshot.toObject(Group.class);
                                    arrayResult.add(group);
                                }
                                s.setResult(arrayResult);

                            }
                        });
                        System.out.println(arrayResult);

                    }
                });
        return s.getTask();
    }

    public void addUserToGroup(String userId,String groupId)
    {
        DocumentReference ref=instance.piczDatabaseFirestore.collection("groups").document(groupId);
        ref.update("usersList",FieldValue.arrayUnion(userId));
    }

    public Task<Group> getSingleGroup(String groupId)
    {
        final TaskCompletionSource s = new TaskCompletionSource<Group>();

        instance.piczDatabaseFirestore.collection("groups").document(groupId).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Group group=documentSnapshot.toObject(Group.class);
                s.setResult(group);
            }
        });
        return s.getTask();
    }



    public Task<Boolean> isUserExists(String groupId, final String userId)
    {
        final TaskCompletionSource s = new TaskCompletionSource<Boolean>();

        instance.piczDatabaseFirestore.collection("groups").document(groupId).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if(documentSnapshot.contains(userId))
                    s.setResult(false);
                else
                    s.setResult(true);
            }
        });
        return s.getTask();
    }

    public void updateUserName(String userId,String newName)
    {
        instance.piczDatabaseFirestore.collection("users").document(userId).update("username",newName);
    }

    public Task<ArrayList<String>> getUsersFromGroup(String groupId)
    {
        final TaskCompletionSource s = new TaskCompletionSource<String>();

        instance.piczDatabaseFirestore.collection("groups").document(groupId).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Group group=documentSnapshot.toObject(Group.class);
                ArrayList<String> arrayResult=new ArrayList<>();
                arrayResult=group.getUsersList();
                s.setResult(arrayResult);
            }

        });
        return s.getTask();
    }

    public Task<String> getUserName(String userId)
    {
        final TaskCompletionSource s = new TaskCompletionSource<String>();

        instance.piczDatabaseFirestore.collection("users").document(userId).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                User user=documentSnapshot.toObject(User.class);
                s.setResult(user.getUsername());
            }
        });
        return s.getTask();
    }

    public Task<ArrayList<String>> getUserNameList(final ArrayList<String> idList)
    {
        final TaskCompletionSource s = new TaskCompletionSource<String>();
        final ArrayList<String> arrayResult=new ArrayList<>();

        instance.piczDatabaseFirestore.collection("users").get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                for(DocumentSnapshot documentSnapshot:queryDocumentSnapshots.getDocuments())
                {
                    User user=documentSnapshot.toObject(User.class);
                    if(idList.contains(user.getFirebaseUserId()))
                        arrayResult.add(user.getUsername());
                }
                s.setResult(arrayResult);
            }
        });
        return s.getTask();

    }

    public Task<Boolean> isAdmin(String groupId, final String userId)
    {
        final TaskCompletionSource s = new TaskCompletionSource<Boolean>();

        instance.piczDatabaseFirestore.collection("groups").document(groupId).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                Group group=documentSnapshot.toObject(Group.class);
                if(group.getAdminsList().contains(userId))
                {
                    s.setResult(true);
                }else
                    s.setResult(false);


            }
        });
        return s.getTask();

    }

    public Task<String> getUserImage(String userId)
    {
        final TaskCompletionSource s = new TaskCompletionSource<String>();

        instance.piczDatabaseFirestore.collection("users").document(userId).get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                User user=documentSnapshot.toObject(User.class);
                s.setResult(user.getIamgeUriString());

            }
        });
        return s.getTask();
    }

    public void uploadImageToFirestore(Image image,String imageid,String groupId)
    {
        instance.piczDatabaseFirestore.collection("images").
                document(imageid).set(image);
        instance.piczDatabaseFirestore.collection("groups")
                .document(groupId).update("imagesList", FieldValue.arrayUnion(imageid));
    }
}
