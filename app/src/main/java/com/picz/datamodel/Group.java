package com.picz.datamodel;

import android.net.Uri;

import com.google.firebase.auth.FirebaseUser;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;


public class Group implements Serializable {

    private String groupId;
    private String groupName;
    private String groupImageUriString;
    private ArrayList<String> usersListId;
    private ArrayList<String> imagesList;
    private String groupCreatorId;
    private ArrayList<String> adminsListId;


    public Group() {}

    public Group(String groupName)
    {
        this.groupName = groupName;
        this.groupImageUriString = null;
        this.usersListId = null;
        this.imagesList = null;
        this.groupCreatorId = null;
        this.adminsListId = null;
        this.groupId=new RandomString().nextString();
    }

    public Group(String groupName, String uri)
    {
        this.groupName = groupName;
        this.groupImageUriString = uri;
        this.usersListId = null;
        this.imagesList = null;
        this.groupCreatorId = null;
        this.adminsListId = null;
        this.groupId=new RandomString().nextString();
    }


    public Group(String groupName, String groupImageUri, ArrayList<String> usersList, ArrayList<String> imagesList,
                 String groupCreatorId, ArrayList<String> adminsList, ZonedDateTime groupCreationTime) {
        this.groupName = groupName;
        this.groupImageUriString = groupImageUri;
        this.usersListId = usersList;
        this.imagesList = imagesList;
        this.groupCreatorId = groupCreatorId;
        this.adminsListId = adminsList;
        this.groupId=new RandomString().nextString();
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupImageUri() {
        return groupImageUriString;
    }

    public void setGroupImageUri(String groupImageUri) {
        this.groupImageUriString = groupImageUri;
    }

    public ArrayList<String> getUsersList() {
        return usersListId;
    }

    public void setUsersList(ArrayList<String> usersList) {
        this.usersListId = usersList;
    }

    public ArrayList<String> getImagesList() {
        return imagesList;
    }

    public void setImagesList(ArrayList<String> imagesList) {
        this.imagesList = imagesList;
    }

    public String getGroupCreator() {
        return groupCreatorId;
    }

    public void setGroupCreator(String groupCreatorId) {
        this.groupCreatorId = groupCreatorId;
    }

    public ArrayList<String> getAdminsList() {
        return adminsListId;
    }

    public void setAdminsList(ArrayList<String> adminsList) {
        this.adminsListId = adminsList;
    }

    public String getGroupId() {
        return groupId;
    }
    public void setAdmins(ArrayList<User>users){}
    public void addImage(Uri imageUri){}
    public void deleteImage(Uri imageUri){}
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
}
