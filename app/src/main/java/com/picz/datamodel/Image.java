package com.picz.datamodel;

import android.net.Uri;

import com.google.firebase.auth.FirebaseUser;

import java.time.ZonedDateTime;
import java.util.ArrayList;

public class Image {

    private String imageId;
    private String imageTitle;
    private int likeCount;
    private ArrayList<String> likesId;
    private String userUploadedId;

    public Image(){}

    public Image(String imageId, String imageTitle, int likeCount,
                 ArrayList<String> likes, String userUploaded) {

        this.imageId=imageId;
        this.imageTitle = imageTitle;
        this.likeCount = likeCount;
        this.likesId = likes;
        this.userUploadedId = userUploaded;
        this.imageId=new RandomString().nextString();
    }



    public String getImageTitle() {
        return imageTitle;
    }

    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }

    public int getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    public ArrayList<String> getLikes() {
        return likesId;
    }

    public void setLikes(ArrayList<String> likes) {
        this.likesId = likes;
    }

    public String getUserUploaded() {
        return userUploadedId;
    }

    public void setUserUploaded(String userUploaded) {
        this.userUploadedId = userUploaded;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }
}
