package com.picz.datamodel;

import android.net.Uri;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;

import androidx.annotation.NonNull;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class PiczStorage {

    private StorageReference piczDatabaseStorage;
    private StorageReference piczDatabaseStorageProfile;
    private StorageReference piczDatabaseStorageGroupImage;
    static PiczStorage storageInstance = null;

    public PiczStorage() {
        piczDatabaseStorage = FirebaseStorage.getInstance().getReference("images");
        piczDatabaseStorageProfile=FirebaseStorage.getInstance().getReference("profile_images");
        piczDatabaseStorageGroupImage=FirebaseStorage.getInstance().getReference("group_images");
    }

    public static PiczStorage getInstance() {
        if (storageInstance == null) {
            storageInstance = new PiczStorage();
        }
        return storageInstance;
    }

    public void uploadImageFromGroup(String uri, final String imageTitle, final String userId, String groupId)
    {
        final String imageId = new RandomString().nextString();
        storageInstance.piczDatabaseStorage.child(imageId).putFile(Uri.parse(uri));
        Image image=new Image(imageId,imageTitle,0,new ArrayList<String>(),userId);
        PiczDatabase.getInstance().uploadImageToFirestore(image,imageId,groupId);
    }
}
