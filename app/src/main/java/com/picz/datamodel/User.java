package com.picz.datamodel;

import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;

public class User {

    private String firbaseUserId;
    private String username;
    private String iamgeUriString;
    private ArrayList<String> groupsId;

    public User() {}

    public User(String firbaseUserId, String username, String iamgeUriString, ArrayList<String> groupsId) {
        this.firbaseUserId = firbaseUserId;
        this.username = username;
        this.iamgeUriString = iamgeUriString;
        this.groupsId = groupsId;
    }

    public String getFirebaseUserId() {
        return firbaseUserId;
    }

    public void setFirebaseUserId(String firbaseUserId) {
        this.firbaseUserId = firbaseUserId;
    }

    public ArrayList<String> getGroups() {
        return groupsId;
    }

    public void setGroups(ArrayList<String> groups) {
        this.groupsId = groups;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIamgeUriString() {
        return iamgeUriString;
    }

    public void setIamgeUriString(String iamgeUriString) {
        this.iamgeUriString = iamgeUriString;
    }

    public void quitGroup(Group group){}

}
